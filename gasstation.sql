-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-06-2023 a las 21:31:46
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gasstation`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `combustible`
--

CREATE TABLE `combustible` (
  `id` bigint(20) NOT NULL,
  `id_vehiculo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `litros` varchar(50) COLLATE utf8_bin NOT NULL,
  `costo` varchar(50) COLLATE utf8_bin NOT NULL,
  `km_inicial` varchar(50) COLLATE utf8_bin NOT NULL,
  `km_final` varchar(50) COLLATE utf8_bin NOT NULL,
  `no_ticket` varchar(50) COLLATE utf8_bin NOT NULL,
  `comision` varchar(200) COLLATE utf8_bin NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_registro` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(150) COLLATE utf8_bin NOT NULL,
  `rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `email`, `password`, `rol`) VALUES
(1, 'world', 'zoro@gmail.com', '123', 1),
(2, 'world2', 'zoro2@gmail.com', '1232', 1),
(3, 'world23', 'zoro2@gmail.com3', '12323', 1),
(4, 'world23w', 'zoro2@gmail.com3w', '12323w', 1),
(5, 'world23wff', 'zoro2@gmail.com3wff', '12323wff', 1),
(6, 'jon', 'zoro2@gmail.com3wff', '$2a$10$YXYmF47pqvAQAdXSJ9T7WeoTt82q3sZasl02OO4jFxAywlfLFxX..', 1),
(11, 'jon2', 'zoro2@gmail.com3wff2', '$2a$10$6Wf111b0d/abIl/Z4G20xe0EX21eAaxzzskbmG6auLc0mBlBiRdCK', 1),
(12, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$UnsQbHzDOZCVxXEbxhdCQeh0qhzNcROXhq6zSJVBc444IL.34mKHG', 1),
(13, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$B2jqXSX2OZvunBHrPwLthevpMDf45AvVuSBxBDBn68ucuvWJJVJJ2', 1),
(14, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$MyYgBN4WKCHNOx85wVtIg.MXqisZJRQNB6yelSukk8SsSiOQQ.JXi', 1),
(15, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$NYcvnxB7cir/yPQUsi3pgejzAs/wbh3K9j0YwoaAv8o08J9O5CXEO', 1),
(16, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$zFs9D2arGzDbHHn/Lk.rVe98X0VOU/yufesBODQR3uA9vMS7unTW6', 1),
(17, 'juan', 'juan@gmail.com', '$2a$10$Pd6dJ9ESKFNyPF/HkBC2JeWP.uHwa2B7YyxhximFk7pxVonAX5JLq', 1),
(18, 'hil', 'hil@gmail.com', '$2a$10$oEVhJ3MQn6aIdHcQnGffmu.NU3N7PaFKbdgtuOkqK5K7BIVeD7JlG', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE `vehiculo` (
  `id` int(11) NOT NULL,
  `marca` varchar(50) COLLATE utf8_bin NOT NULL,
  `submarca` varchar(50) COLLATE utf8_bin NOT NULL,
  `modelo` varchar(50) COLLATE utf8_bin NOT NULL,
  `tipo` varchar(50) COLLATE utf8_bin NOT NULL,
  `color` varchar(50) COLLATE utf8_bin NOT NULL,
  `no_motor` varchar(50) COLLATE utf8_bin NOT NULL,
  `serie` varchar(50) COLLATE utf8_bin NOT NULL,
  `placas` varchar(50) COLLATE utf8_bin NOT NULL,
  `inventario` varchar(50) COLLATE utf8_bin NOT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vehiculo`
--

INSERT INTO `vehiculo` (`id`, `marca`, `submarca`, `modelo`, `tipo`, `color`, `no_motor`, `serie`, `placas`, `inventario`, `id_usuario`) VALUES
(1, 'ferrari', 'Enzo', '2008', 'DEPORTIVO', 'RED', '4555', '84458', 'feh88749', '84558', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `combustible`
--
ALTER TABLE `combustible`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `combustible`
--
ALTER TABLE `combustible`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
