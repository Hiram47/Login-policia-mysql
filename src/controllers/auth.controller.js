import {connect} from '../db.js';
import bcryptjs from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { TOKEN_SECRET } from '../config.js';


export const register = async(req,res)=>{
    try {
    const {email,password,username,rol}= req.body

    const db=await connect()
     const [userFound]=await db.query('SELECT * FROM usuarios WHERE email=? ',[email])
     console.log(userFound[0]);

     if (userFound[0]) return res.status(400).json(['The email already exists'])

    const passwordHash=await bcryptjs.hash(password,10)

    console.log(email,password,passwordHash,username,rol);

        const [result]=await db.query('INSERT INTO `usuarios`( `username`, `email`, `password`, `rol`) VALUES (?,?,?,?)',[username,email,passwordHash,rol])
        const [res2] =await db.query('SELECT id,username,email from usuarios order by id desc limit 1')

        const token =jwt.sign({id:res2[0].id},TOKEN_SECRET,{expiresIn:'1d'}
        // ,(error,token)=>{
        //     if(error){
        //         console.log(error); 
        //         res.json({token})} 
        // }
        )

        res.cookie('token',token)

        res.json(res2[0])
       //res.json({token})
        

        // await db.beginTransaction(async db =>{
        //     const row = await db.
        // })
        
    } catch (error) {
        console.log(error);
        
    }
  

}

export const login = async(req,res)=>{
    
    const {email,password}= req.body

    //const passwordHash=await bcryptjs.hash(password,10)

    console.log(email,password);
    try {
        const db=await connect()
       // const [result]=await db.query('INSERT INTO `usuarios`( `username`, `email`, `password`, `rol`) VALUES (?,?,?,?)',[username,email,passwordHash,rol])
        const [res2] =await db.query('SELECT * from usuarios where email =?',[email])

        if (!res2[0]) {
            res.status(400).json(['Incorrect User'])
        }

        const isMatch=await bcryptjs.compare(password,res2[0].password)

        if (!isMatch) {
            res.status(401).json({message:'Incorrect Password'})
        }

        const token =jwt.sign({id:res2[0].id},TOKEN_SECRET,{expiresIn:'1d'})
       

        res.cookie('token',token
         ,{
            sameSite:'lax'
          ,secure:false
        //   ,httpOnly:true
        }
        )

        res.json(res2[0])
       //res.json({token})
        
        
    } catch (error) {
        console.log(error);
        //res.json(['Error en API or DB'])
        // res.status(400).json(['Error en API LOGIN'])
        
    }
  

}

export const logout = (req,res)=>{
    res.cookie('token','',{expires: new Date(0)})

    return res.sendStatus(200)


}

export const profile = async(req,res) =>{

    try {        
        const db=await connect()
        const [res2] =await db.query('SELECT * from usuarios where id =?',[req.user.id])

        if (!res2[0]) {
            
            res.status(400).json({message:'User not found'})
            
        }

        console.log(req.user);
        res.send(res2[0])
    } catch (error) {

        console.log(error);
        
    }

}

export const verifyTokenReact = async(req,res)=>{
    const {token}=req.cookies
    if(!token) return res.status(401).json({message:'Unauthorized'})

    jwt.verify(token,TOKEN_SECRET,async(err,user)=>{
        if(err) return res.status(401).json({message:'Unauthorized'})
        try {
            const db=await connect()
            console.log(user.id);
        const [res2] =await db.query('SELECT * from usuarios where id =?',[user.id])
        if (!res2[0]) {
            
            res.status(400).json({message:'Unauthorized'})
            
        }
        return res.json(res2[0])
            
        } catch (error) {
            console.log(error);
            
        }
    })

}