import {connect} from '../db.js'

export const getVehicles=async(req,res)=>{

    try {
        const db=await connect()
       const [result]= await db.query('SELECT * FROM vehiculo')
       res.status(200).json(result)
        
    } catch (error) {
        console.log(error);
        res.status(500).json({message:'Error en la bd'})
        
    }



}

export const getVehicle= async (req,res)=>{
    try {
        let id=req.params.id;
        const db= await connect();

        const [result]=await db.query(`SELECT * FROM vehiculo where id=?`,[id])
        console.log(result[0]);
        res.status(200).json(result[0])

    } catch (error) {
        console.log(error);
        
        
    }

}

export const getCarro = async(req,res)=>{
    try {

        let placa=req.params.placa;
        const db= await connect();

        //query helps to search lower an upper texts
        const [result] = await db.query(`SELECT * FROM vehiculo where placas COLLATE UTF8_GENERAL_CI like ? or inventario COLLATE UTF8_GENERAL_CI like ? or no_motor COLLATE UTF8_GENERAL_CI like ? `,[`%${placa}%`,`%${placa}%`,`%${placa}%`])
        console.log(result);
        
        res.json(result)
        
    } catch (error) {
        console.log(error);
        
    }
}

export const saveVehicle= async(req,res)=>{

    try {
        
        const db = await connect()
        console.log(req.user,'noe');
        let id_usuario=req.user.id
    
        const result = await db.query("INSERT into vehiculo set ?",[req.body])    
        console.log(result);

        res.status(200).json('vehicle was saved');   

    } catch (error) {
        console.log(error);
        
    }


}

export const updateVehicle = async (req,res) =>{
    try {
        let id= req.params.id
        const db= await connect();

        const result =await db.query('UPDATE vehiculo SET ? where id=?',[req.body,id])

        console.log(result);

        res.status(204).json({message:'vehicle updated successfuly'})
        
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
        
    }
}

export const deleteVehicle = async (req,res) =>{
    try {
        let id= req.params.id
        const db= await connect();

        const [result]= await db.query('DELETE FROM vehiculo WHERE id=?',[id]);
        console.log(result);
        res.status(201).json({message:'vehiculo deleted successfuly'})

        
    } catch (error) {
        console.log(error);
        
    }
}


// export const updateTask=async(req,res)=>{

//     const db= await connect();

//     const result =await db.query('UPDATE tasks SET ? where id=?',[req.body,req.params.id])
//     console.log(result);
//     res.sendStatus(204)

// }


// export const deleteTask=async(req,res)=>{

//     const db = await connect();
    
//     await db.query('DELETE FROM tasks WHERE id=?',[req.params.id])

//     res.sendStatus(204)

// }