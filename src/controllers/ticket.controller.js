import {connect} from '../db.js'

export const getTicketByUser= async(req,res)=>{
    try {

        let id=req.params.id;
        const db= await connect();

        //query helps to search lower an upper texts
        const [result] = await db.query(`SELECT * FROM combustible where id_vehiculo= ? `,[id])
        console.log(result);
        
        res.json(result)
        
    } catch (error) {
        
        console.log(error);
        
    }


}


export const saveTicket= async(req,res)=>{

    try {
        
        const db = await connect()
        console.log(req.user,'noe');
        let id_usuario=req.user.id
    
        const result = await db.query("INSERT into combustible set ?",[req.body])    
        console.log(result);

        res.status(200).json('ticket was saved');   

    } catch (error) {

        console.log(error);
        
    }


}

export const deleteTicket= async(req,res)=>{
    try {
        const db = await connect()
        let id= req.params.id
        console.log(id);
        const result = await db.query(`DELETE FROM combustible WHERE id =?`,[id] )
        console.log(result);
        res.status(200).json('ticket was deleted');  
        
    } catch (error) {
        console.log(error);
        
    }
}

export const reporteByVehiculo = async (req,res)=>{
    try {
        const db=await connect()
        let id= req.params.id

        const [result] = await db.query('SELECT * FROM combustible where id_vehiculo =? and fecha BETWEEN ? and ?',[req.body.id_vehiculo,req.body.inicial,req.body.final])
        console.log(result);
        
        res.json(result)
        
    } catch (error) {
        console.log(error);
        
    }
}

export const puestoReporte = async (req,res)=>{
    try {
        const db=await connect()
        let id= req.params.id

        const [result] = await db.query('SELECT * FROM puesto',)
        console.log(result[0]);
        
        res.json(result[0])
        
    } catch (error) {
        console.log(error);
        
    }
}

export const reporteAllCars = async (req,res) =>{
    try {
        const db= await connect()

        // const [result] = await db.query("SELECT c.id_vehiculo, CONCAT_WS(' ',v.marca,v.submarca,v.modelo,v.color,v.placas,v.serie) AS carro,SUM(c.litros) AS litros, SUM(c.costo) AS costos , SUM(c.litros)*SUM(c.costo) AS total FROM combustible c INNER JOIN vehiculo v ON v.id=c.id_vehiculo WHERE  fecha BETWEEN ? AND ? GROUP BY c.id_vehiculo",[req.body.inicial,req.body.final])
        // new query
        
        const [result] = await db.query("SELECT c.id_vehiculo,CONCAT_WS(' ',v.marca,v.submarca,v.modelo,v.color,v.placas,v.serie) AS carro,sum(c.litros) AS litros, SUM(c.costo) AS costos FROM combustible c INNER JOIN vehiculo v ON v.id=c.id_vehiculo WHERE  fecha BETWEEN ? AND ? GROUP BY c.id_vehiculo",[req.body.inicial,req.body.final])

        console.log(result);

        res.json(result)
        
    } catch (error) {
        console.log(error);
        
    }
}