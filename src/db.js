import mysql from 'mysql2/promise'
import {config} from './config.js'

export const connect = async () =>{
    try {
        
        console.log('db is running');
        return await mysql.createConnection(config)
        
    } catch (error) {
        console.log(error);
        console.log('Error en conectar db');
        
    }
}