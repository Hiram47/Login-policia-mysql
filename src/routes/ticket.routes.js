import {Router} from 'express'
import { deleteTicket, getTicketByUser, puestoReporte, reporteAllCars, reporteByVehiculo, saveTicket } from '../controllers/ticket.controller.js'
import { authRequired } from '../middlewares/validateToken.js'


const router=Router()

router.post('/ticket',authRequired,saveTicket)
router.get('/ticket/:id',authRequired,getTicketByUser)
router.delete('/deleteticket/:id',authRequired,deleteTicket)
router.post('/fechavehiculo',authRequired,reporteByVehiculo)
router.get(`/puesto`,authRequired,puestoReporte)
router.post('/fechareporte',authRequired,reporteAllCars)

export default router