import React from 'react'

import ssp from '../assets/ssp.png'
const HomePage = () => {
  return (
    <div className='flex h-[calc(100vh-100px)] items-center justify-center'>
      <h2 className='text-4xl'>Secretaria de Seguridad Pública</h2>
      
      
      <img className='' src={ssp} alt="" />

    </div>
  )
}

export default HomePage