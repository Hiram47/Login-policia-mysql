import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate, useParams } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'


import { useTasks } from '../context/TaskContext'

const TaskFormPage = () => {

    const {register,handleSubmit,setValue}=useForm()

    const navigate = useNavigate()
    const params=useParams()
    console.log(params.id);

    const {createTask,getTask,updateTask}=useTasks()
   
    const {user}=useAuth()
    console.log(user);

    useEffect(()=>{

        async function loadTask (){

            if (params.id) {
                const task=await getTask(params.id)
                console.log(task);
                setValue('marca',task.marca)
                setValue('submarca',task.submarca)
                setValue('modelo',task.modelo)
                setValue('tipo',task.tipo)
                setValue('color',task.color)
                setValue('no_motor',task.no_motor)
                setValue('serie',task.serie)
                setValue('placas',task.placas)
                setValue('inventario',task.inventario)     
                setValue('usuario_modifico',user.email) 
            }else{
                setValue('id_usuario',user.email)           
            }
        }
        loadTask()

    },[])


    const onSubmit= handleSubmit(async (data) =>{

        if (params.id) {
             await updateTask(params.id,data)
        }else{

            await createTask(data);      
        }
         navigate('/vehiculo')
        
    })

  


  return (
    <div className='bg-zinc-800 max-w-md w-full p-10 rounded-md'>
        <form onSubmit={onSubmit}>
            <label htmlFor="">Marca</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Marca' {...register('marca')} autoFocus />


            <label htmlFor="">Submarca</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Submarca' {...register('submarca')}  />

            <label htmlFor="">Modelo</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Modelo' {...register('modelo')}  />

            <label htmlFor="">Tipo</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Tipo' {...register('tipo')}  />

            <label htmlFor="">Color</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Color' {...register('color')}  />

            <label htmlFor="">No. Economico</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='No_economico' {...register('no_motor')}  />

            <label htmlFor="">Serie</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Serie' {...register('serie')}  />

            <label htmlFor="">Placa</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Placa' {...register('placas')}  />

            <label htmlFor="">Inventario</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Inventario' {...register('inventario')}  />

            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="hidden" placeholder='Usuario' {...register('id_usuario')}  />
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="hidden" placeholder='Modifico' {...register('usuario_modifico')}  />

            <button className='bg-indigo-500 px-3 py-2 rounded-md'>Save</button>
        </form>
    </div>
  )
}

export default TaskFormPage