import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTicket } from '../context/TicketContext'

export const AllReportsPage = () => {
    const {register,handleSubmit}=useForm()
    const {cars,getCarrosReporte}=useTicket()
    const [sumatotal, setsumatotal] = useState(0)

    let suma=0;


    const onSubmit=handleSubmit(data=>{
        const {inicial,final}= data
        console.log(data);
        if (inicial > final) {
            console.log('err');
            alert('Las fechas son incorrectas')

        }else{
           // getVehiculobyReporte(data)
           getCarrosReporte(data)

            console.log(inicial,final);
           // setfechas({inicial,final})
           

        }
    }) 

  return (
    <div className='bg-zinc-800 w-full p-10 rounded-md'>
        <h2 className='text-2xl'>Reportes:</h2>
        <hr className='py-2' />
        <form onSubmit={onSubmit}>
        <div className='grid sm:grid-cols-2 md:grid-cols-3 gap-2 '>
            {/* <div>

                 <label htmlFor="">Fecha inicial</label>
              <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' placeholder='Fecha inicial' type="text" {...register('id_vehiculo')} required />
            </div> */}

                <div>

                   <label htmlFor="">Fecha inicial</label>
                    <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' placeholder='Fecha inicial' type="date" {...register('inicial')} required  />
                </div>
                <div>
                    <label htmlFor="">Fecha final</label>
                    <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' placeholder='Fecha final' type="date" {...register('final')} required />
                </div>



            </div>
            <button className='bg-sky-600 p-2 my-2 rounded-md hover:bg-sky-500' >Generar</button>

        </form>

        <table className= ' bg-zinc-800 max-w-md w-full p-10 rounded-lg    min-w-full text-center text-md font-light  '>
            <thead className='border-b bg-neutral-50 font-medium dark:border-neutral-500 dark:text-neutral-800'>
            <tr className='text-black'>
              <th className='col-md-1'>#</th>

              <th className='col-md-1  font-bold'>Vehiculo</th>
              <th className='col-md-1'>litros</th>
              <th className='col-md-1'>costo</th>
              <th className='col-md-1'>Total</th>
              {/* <th className='col-md-1'>Total cantidad</th> */}
         
            
              
            </tr>

            </thead>
            <tbody>

                {

                        cars.map((c,i)=> (
                            <tr key={c.id_vehiculo} >
                                
                                <td>{i+1}</td>
                                <td>{c.carro?.toUpperCase()}</td>                        
                                <td>{c.litros} lts.</td>
                                <td>$ {new Intl.NumberFormat().format(c.costos)}</td>
                                <td> $ {new Intl.NumberFormat().format(suma+=c.costos)}</td>
                                {/* <td> $ {new Intl.NumberFormat().format(c.total)}</td>
                                <td>$ {new Intl.NumberFormat().format(suma+=c.total)}</td> */}
                                

                                
                        </tr>
                        ))
                        // .reduce((a,b)=>a+b,0)


                    }
                    
            </tbody>
            </table>
                

    </div>
  )
}
