import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useParams } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'
import { useTicket } from '../context/TicketContext'

import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import {es} from 'dayjs/locale/es-mx'
dayjs.locale('es')
dayjs.extend(utc)

export const TicketFormPage = () => {
    const params=useParams()
     const {user}=useAuth()
    const {register,setValue,handleSubmit}=useForm()
    const {prueba,getTickets,tikets,createTiketGas,deleteTicketGas}=useTicket()
    const [load, setload] = useState(true)

    console.log(prueba());

    useEffect(() => {
        console.log(params.id);
        let today = new Date().toLocaleDateString()
        
        async function loadticket(){
           const getallTickets=await getTickets(params.id)
           console.log(getallTickets);
           setValue('id_vehiculo',params.id)
           setValue('usuario_modifico',user.email)
           setValue('fecha_registro',today)

        }
        loadticket()
   
    }, [load])//para cargar la tabla al ingresar un dato del formulario
   
    

    

    const onSubmit =handleSubmit(async tickets =>{

       const res= await createTiketGas(tickets)
       console.log(res);
       //cargar datos de la tabla
       setload(false)

    })

    const ondelete = async( id)=>{
        await deleteTicketGas(id)
        console.log(id);
        setload(false)
    }
    
  return (
    <div className=' '>
        <form className='' onSubmit={onSubmit}>
            <div className='grid sm:grid-cols-2 md:grid-cols-3 gap-2 '>

            <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="hidden" placeholder='idVehiculo' {...register('id_vehiculo')}  />
            <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="hidden" placeholder='Usuario modifico' {...register('usuario_modifico')} autoFocus   />

                
                <input className=' bg-zinc-700 rounded-md py-2 mx-2 ' type="date" placeholder='fecha registro' {...register('fecha')} required/>
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='Litros' {...register('litros')} autoFocus   />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='Costo' {...register('costo')} />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='Km'{...register('km_final')} />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='No Ticket' {...register('no_ticket')} />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='Lugar de comision' {...register('comision')} />

                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='usuario' {...register('id_usuario')}  />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="hidden" placeholder='fecha registro' {...register('fecha_registro')}  />
                <input className='bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='observacion' {...register('observaciones')}/>

            </div>
            <div className='mb-3'>
                <button className='w-full bg-indigo-600 py-2 px-1 my-3 rounded-md'>Guardar</button>

            </div>
            <div className='mb-6'>
                {/* <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" /> */}
            </div>

        </form>


        <table className= ' bg-zinc-800 max-w-md w-full p-10 rounded-lg    min-w-full text-center text-md font-light  '>
            <thead className='border-b bg-neutral-50 font-medium dark:border-neutral-500 dark:text-neutral-800'>
            <tr>
              <th className='col-md-1'>#</th>

              <th className='col-md-1  font-bold'>Fecha</th>
              <th className='col-md-1'>Litros</th>
              <th className='col-md-1'>Costo</th>
              <th className='col-md-1'>Kilometraje</th>
              <th className='col-md-1'>No.ticket</th>
              <th className='col-md-1'>Comision</th>
              <th className='col-md-1'>Observaciones</th>
              <th className='col-md-1'>Usuario</th>
              <th className='col-md-1'>Eliminar</th>
             
            </tr>

            </thead>
            <tbody>

             
        {
            tikets.map((tiket,i)=>(
                <tr key={tiket.id} >
                    <td>{i+1}</td>
                    <td>{dayjs.utc(tiket.fecha ).locale('es').format('DD/MM/YYYY')}</td>
                    <td>{tiket.litros} lts.</td>
                    <td>${tiket.costo?.toUpperCase()}</td>
                    <td>{tiket.km_final} KM</td>
                    <td>{tiket.no_ticket}</td>
                    <td>{tiket.comision?.toUpperCase()}</td>
                    <td>{tiket.observaciones?.toUpperCase()}</td>
                    <td>{tiket.id_usuario?.toUpperCase()}</td>
                    <td><button className='bg-red-800 hover:bg-red-600 p-1 m-2 rounded-md font-bold' onClick={()=>ondelete(tiket.id)}>Eliminar</button></td>
                </tr>
            ))
        }

            </tbody>

        </table>

        {/* <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScKgKddgFK_uciJi72ukZcMO22L8MyxXpl2q0GSRjBeimIHmg/viewform?embedded=true" width="640" height="612" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe> */}

    </div>
  )
}
