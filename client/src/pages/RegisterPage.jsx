import { useEffect } from 'react'
import {useForm} from 'react-hook-form'
import { Link, useNavigate } from 'react-router-dom'
import {registerRequest} from '../api/auth'
import {useAuth} from '../context/AuthContext'

const RegisterPage = () => {
    const {register,handleSubmit,formState:{errors}}=useForm()

    const {signup,user,isAuthenticaded,errors:registerErrors}=useAuth()
    console.log(user);
    // console.log(user.email);
    const navigate=useNavigate()

    useEffect(() => {
      if (isAuthenticaded) {
        navigate('/vehiculo')
      }
    }, [isAuthenticaded])
    

    const onSubmit=handleSubmit(async(values)=>{ 
      signup(values)
        })

  return (
    <div className='flex h-[calc(100vh-100px)] items-center justify-center'>
    <div className='bg-zinc-800 max-w-md p-10 rounded-md'>
        <h1 className='text-3xl font-bold'> Registro</h1>
        {
            registerErrors.map((error,i) =>(
                <div key={i} className='bg-red-500 p-2 text-white text-center my-2' >{error}</div>
                ))
            }
        <form onSubmit={onSubmit} >
            <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text" placeholder='Username' {...register('username',{required:true}  )} />
            {   errors.username &&(<p className='text-red-500' >Username is required</p> ) }


            <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="email" placeholder='Email' {...register('email',{required:true}  )} />
            {   errors.email &&(<p className='text-red-500' >Email is required</p> ) }


            <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="password" placeholder='Password'  {...register('password',{required:true}  )}   />
            {   errors.password &&(<p className='text-red-500' >Password is required</p> ) }



            <button type='submit' >Register</button>
        </form>

        <p className='flex gap-x-2 justify-between'>
            Already have an account? <Link to='/login' className='text-sky-500'>Login</Link>
        </p>

    </div>
    </div>
  )
}

export default RegisterPage