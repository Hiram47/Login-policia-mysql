import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useParams } from 'react-router-dom'
import { useTicket } from '../context/TicketContext'

import { useAuth } from '../context/AuthContext'

import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import {es} from 'dayjs/locale/es-mx'
dayjs.locale('es')
dayjs.extend(utc)

import {PDFViewer, renderToString} from '@react-pdf/renderer';
import { PDF } from '../pdf/PDF'
import { useTasks } from '../context/TaskContext'

export const ReportePage = () => {

    const {register,handleSubmit,setValue}=useForm()

    const params=useParams()

    const {user}=useAuth()
    console.log(user);

    const {getVehiculobyReporte,car,puestoPolicia,getPuestoReporte}=useTicket()
    const {getTask}=useTasks()
    const [vehiculo, setvehiculo] = useState({})
    const [fechas, setfechas] = useState({})
    const [puestopdf, setpuestopdf] = useState({})

    useEffect(() => {
     console.log(params);
     setValue('id_vehiculo',params.id)
    }, [])
    
    
    useEffect(() => {

        async function getCar(){

            const rescar=await getTask(params.id)
            console.log(rescar);
            setvehiculo(rescar)
        }
        getCar()
    
    }, [])

    useEffect(() => {
        console.log(puestoPolicia);
        async function getpuesto(){
            const data= await getPuestoReporte()
            console.log(data);
            setpuestopdf(data)
        }
        getpuesto()
        
    }, [])
    
    console.log(puestoPolicia)
    

    const onSubmit=handleSubmit(data=>{
        const {inicial,final}= data
        console.log(data);
        if (inicial > final) {
            console.log('err');
            alert('Las fechas son incorrectas')

        }else{
            getVehiculobyReporte(data)

            console.log(inicial,final);
            setfechas({inicial,final})
           

        }
    }) 
  return (
    <div className='bg-zinc-800 w-full p-10 rounded-md'>
        <form onSubmit={onSubmit}>
            <h2 className=' text-3xl font-bold py-4'>Reporte por vehiculo</h2>
            <div className='grid sm:grid-cols-2 md:grid-cols-3 gap-2 '>
            {/* 
            <div>

                 <label htmlFor="">Id</label>
              <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' placeholder='Fecha inicial' type="text" {...register('id_vehiculo')} required />
            </div> 
            */}

                <div>

                   <label htmlFor="">Fecha inicial</label>
                    <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' placeholder='Fecha inicial' type="date" {...register('inicial')} required  />
                </div>
                <div>
                    <label htmlFor="">Fecha final</label>
                    <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' placeholder='Fecha final' type="date" {...register('final')} required />
                </div>



            </div>
            <button className='bg-sky-600 p-2 my-2 rounded-md' >Buscar</button>
        </form>


        <table className= ' bg-zinc-800 max-w-md w-full p-10 rounded-lg    min-w-full text-center text-md font-light  '>
            <thead className='border-b bg-neutral-50 font-medium dark:border-neutral-500 dark:text-neutral-800'>
            <tr>
              <th className='col-md-1'>#</th>

              <th className='col-md-1  font-bold'>Fecha</th>
              <th className='col-md-1'>litros</th>
              <th className='col-md-1'>costo</th>
              <th className='col-md-1'>KM</th>
              <th className='col-md-1'>Ticket</th>
              <th className='col-md-1'>Comision</th>
              <th className='col-md-1'>observaciones</th>
              <th className='col-md-1'>Usuario</th>
            
              
            </tr>

            </thead>
            <tbody>
                {

                    car.map((c,i)=> (
                        <tr key={c.id} >
                        <td>{i+1}</td>
                        <td>{dayjs.utc(c.fecha ).locale('es').format('DD/MM/YYYY')}</td>
                        <td>{c.litros} lts.</td>
                        <td>{c.costo?.toUpperCase()}</td>
                        <td>{c.km_final} KM</td>
                        <td>{c.no_ticket}</td>
                        <td>{c.comision?.toUpperCase()}</td>
                        <td>{c.observaciones?.toUpperCase()}</td>
                        <td>{c.id_usuario?.toUpperCase()}</td>
                        {/* <td><button className='bg-red-800 hover:bg-red-600 p-1 m-2 rounded-md font-bold' onClick={()=>ondelete(tiket.id)}>Eliminar</button></td> */}
                    </tr>
                    ))

                }

             

            </tbody>

        </table>


        <div className='py-10'>
            {fechas.inicial &&  car.length >0?

            <PDFViewer  style={{height:'80vh',width:'50%'}}>
                <PDF key={puestoPolicia.id} car={car} vehiculo={vehiculo} fechas={fechas} puestoPolicia={puestoPolicia} user={user} />

            </PDFViewer>
            :
            <>Ingresar busqueda para ver PDF</>
            }

        </div>


    </div>
  )
}
