import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { TaskTable } from '../components/TaskTable'
import { useAuth } from '../context/AuthContext'
import { useTasks } from '../context/TaskContext'

const TaskPage = () => {
    const {user}= useAuth()
    const {getTasks,tasks,getTaskPlaca}=useTasks()

    const {register,handleSubmit,setValue}=useForm()

    const onSubmit = handleSubmit(data =>{
        if (!data.placa==null || !data.placa =='') {
            
            console.log(data);
            getTaskPlaca(data.placa)
        }
        
       return;

    })

    useEffect(() => {
        getTasks()
     
    }, [])

    if(tasks.length ===0) return (<h2>No hay registros</h2>)
    
  return (
    <div className='m-5 bg-zinc-900 p-5 rounded-lg'>
        <form onSubmit={onSubmit}>

        <input className='w-full bg-zinc-700 text-white px-4 py-2 my-3 rounded-lg ' type="text" placeholder='buscar vehiculo' {...register('placa')} autoFocus  />
        <button className='w-full bg-sky-600 rounded-xl my-2 py-2 font-bold text-base'>Buscar</button>
        </form>
        <table className= ' bg-zinc-800 max-w-md w-full p-10 rounded-lg    min-w-full text-center text-md font-light  '>
            <thead className='border-b bg-neutral-50 font-medium dark:border-neutral-500 dark:text-neutral-800'>
            <tr>
              <th className='col-md-1'>#</th>

              <th className='col-md-1  font-bold'>Marca</th>
              <th className='col-md-1'>Submarca</th>
              <th className='col-md-1'>Modelo</th>
              <th className='col-md-1'>Tipo</th>
              <th className='col-md-1'>Color</th>
              <th className='col-md-1'>Economico</th>
              <th className='col-md-1'>Serie</th>
              <th className='col-md-1'>Placas</th>
              <th className='col-md-1'>Inventario</th>
              <th> Editar</th>
              <th> Accion</th>
              <th>Reporte</th>
            </tr>

            </thead>
            <tbody>

                 {
                 tasks.map((task,i)=>  (

                        <TaskTable task={task} i={i} key={task.id} />
                    ) )}

            </tbody>

        </table>

    </div>
  )
}

export default TaskPage