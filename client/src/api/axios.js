import axios from 'axios'

const instance=axios.create({
    baseURL:'http://10.11.60.124:3999/api',
    withCredentials:true
})

export default instance