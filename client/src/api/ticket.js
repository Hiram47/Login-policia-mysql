import axios from './axios'

export const getTicketRequest = (id) => axios.get(`/ticket/${id}`)

export const createTicketRequest= (tickets) => axios.post('/ticket',tickets)

export const deleteTicketrequest= (id) => axios.delete(`/deleteticket/${id}`)

export const getReporteByvehiculoRequest= (ticket) => axios.post(`/fechavehiculo`,ticket)

export const getReporteAllVehiculesRequest= (ticket) => axios.post('/fechareporte',ticket)

export const getPuestoRequest = () => axios.get('/puesto')