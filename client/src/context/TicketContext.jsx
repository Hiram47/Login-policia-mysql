import { createContext, useContext, useEffect, useState } from "react";
import { createTicketRequest, deleteTicketrequest, getPuestoRequest, getReporteAllVehiculesRequest, getReporteByvehiculoRequest, getTicketRequest } from "../api/ticket";


const TicketContext= createContext();

export const useTicket=()=>{
    const context = useContext(TicketContext);

    if (!context) {
        throw new Error('useticket must be used within a Taskprovider')        
    }

    return context;

    
}


export function TicketProvider({children}){
    
    const [tikets, settikets] = useState([])
    const [car, setcar] = useState([])
    const [puestoPolicia, setpuestoPolicia] = useState({})
    const [cars, setcars] = useState([])
    const prueba=()=>{
        console.log('hola');
    }

   
    const getTickets=async (id)=>{
        try {
          const {data}=await  getTicketRequest(id)
            console.log(data);
            settikets(data)
            
        } catch (error) {
            console.log(error);
            
        }

    }
    

    const createTiketGas= async(data)=>{
        try {
            
           let res= await createTicketRequest(data)
           console.log(res);
        } catch (error) {
            console.log(error);
            
        }

    }

    const deleteTicketGas= async(id)=>{
        try {

            let res= await deleteTicketrequest(id)
            console.log(res.data);
            if(res.data==='ticket was deleted') settikets(tikets.filter(ticket => ticket.id != id))
            
        } catch (error) {

            console.log(error);
            
        }
    }

    const getVehiculobyReporte = async(ticket)=>{
        try {
            let {data} =await getReporteByvehiculoRequest(ticket)
            console.log(data);
            setcar(data)

        } catch (error) {
            console.log(error);
            
        }
    }

    const getPuestoReporte = async()=>{
        try {
            let {data}= await getPuestoRequest()
            console.log(data);
            setpuestoPolicia(data)
            
        } catch (error) {
            console.log(error);
            
        }
    }

    const getCarrosReporte = async(ticket)=>{
        try {
            let {data} = await getReporteAllVehiculesRequest(ticket)
            console.log(data);
            setcars(data)
            
        } catch (error) {
            console.log(error);            
        }
    }



    return(
        <TicketContext.Provider value={{
            prueba,
            getTickets,
            tikets,           
            createTiketGas,
            deleteTicketGas,
            getVehiculobyReporte,
            car,
            getPuestoReporte,
            puestoPolicia,
            getCarrosReporte,
            cars
        }}>
            {children}
        </TicketContext.Provider>
    )
}