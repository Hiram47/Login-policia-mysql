import { createContext, useContext, useEffect, useState } from "react";
import {registerRequest,loginRequest, verifyTokenRequest} from '../api/auth'
import Cookies from 'js-cookie'

const AuthContext = createContext()

export const useAuth = () =>{
    const context = useContext(AuthContext)
    if (!context) {
        throw new Error('useAuth must be used within an AuthProvider')
        
    }
    return context
}

export const AuthProvider = ({children})=>{
    const [user, setuser] = useState(null)
    const [isAuthenticaded, setisAuthenticaded] = useState(false)
    const [errors, seterrors] = useState([])
    const [loading, setloading] = useState(true)

    const signup= async(user) =>{
        try {
              //   console.log(values);
        const res= await registerRequest(user);
        console.log(res.data); 
        setuser(res.data)
        setisAuthenticaded(true)
            
        } catch (error) {
            console.log(error.response.data);
            seterrors(error.response.data)
            
        }
   
    }

    const signin = async (user)=>{

        try {
            const res= await loginRequest(user)
            console.log(res);
            console.log(res.data); 
        setuser(res.data)
        setisAuthenticaded(true)
        } catch (error) {
            if (Array.isArray(error.response.data)) {
                
               // console.log(error.response.data);
               return seterrors(error.response.data) 
            }
            seterrors([error.response.data.message])

        }

    }

    const logout= ()=>{
        Cookies.remove('token')
        setisAuthenticaded(false)
        setuser(null)

    }


    useEffect(() => {
        if (errors.length > 0) {
            const timer=setTimeout(() =>{
                seterrors([])
            },5000)
            return ()=> clearTimeout(timer)
            
        }
     
    }, [errors])

    useEffect(() => {
        async function checkLogin(){

            const cookies=Cookies.get()
            console.log(cookies);
    
            if (!cookies.token) {
                setisAuthenticaded(false)
                setloading(false)
                return setuser(null)
            }
            try {
                    console.log(cookies.token);
                    
                    const res=await verifyTokenRequest(cookies.token)
                    console.log(res.data);
    
                    if(!res.data) {

                        setisAuthenticaded(false)
                        setloading(false)
                        return;
                    } 
    
                    setisAuthenticaded(true)
                    setuser(res.data)
                    setloading(false)
    
                } catch (error) {
                    console.log(error);
                    setisAuthenticaded(false)
                    setuser(null)
                    setloading(false)
                    
                }
                
        }
        checkLogin()
      
    }, [])





    
    

    

    return(
        <AuthContext.Provider value={{
            signup,
            user,
            isAuthenticaded,
            errors,
            loading,
            signin,
            logout
        }}>
            {children}
        </AuthContext.Provider>
    )
}