import React from 'react'
import { Link } from 'react-router-dom';

export const TaskTable = ({task,i}) => {
  return (
    <tr className='border-b dark:border-neutral-500' >

    <td className='whitespace-nowrap  px-4 py-2 font-sans'>{i+1}</td>
      <td>{task.marca.toUpperCase()}</td>
      <td>{task.submarca.toUpperCase()}</td>
      <td>{task.modelo.toUpperCase()}</td>
      <td>{task.tipo.toUpperCase()}</td>
      <td>{task.color.toUpperCase()}</td>
      <td>{task.no_motor.toUpperCase()}</td>
      <td>{task.serie.toUpperCase()}</td>
      <td>{task.placas.toUpperCase()}</td>
      <td>{task.inventario.toUpperCase()}</td>
      <td ><Link className='bg-blue-500 hover:bg-blue-600 rounded-md min-w-full text-white  font-bold p-2' to={`/vehiculo/${task.id}`}><i style={{fontSize:'30px'}} className="fa fa-edit" ></i></Link></td>
      <td ><Link className='bg-green-600 rounded-md min-w-full text-white  font-bold hover:bg-green-700 p-2 ' to={`/combustible/${task.id}`}><i style={{fontSize:'25px'}} className="fa fa-file" ></i></Link></td>
      <td ><Link className='bg-red-700 rounded-md min-w-full text-white  font-bold hover:bg-red-500 p-2 ' to={`/reporte/${task.id}`}><i style={{fontSize:'25px'}} className="fa fa-file-pdf-o" ></i></Link></td>
      {/* <td ><button className='bg-yellow-600 rounded-sm min-w-full text-white  font-bold hover:bg-orange-600 '>Editar</button></td> */}

</tr>  
  )
}
