import React from 'react'
import { Link } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'
import ssp from '../assets/ssp.png'

export const Navbar = () => {

    const {isAuthenticaded,user,logout}=useAuth()

  return (
    <nav className='bg-zinc-700 my-3 flex justify-between py-5 px-10 rounded-lg'>
        <Link to={isAuthenticaded ? '/vehiculo' : '/'}>
            <h1 className='text-3xl font-bold flex justify-between ' ><img style={{width:'38px'}} className='px-1' src={ssp} alt="SSP" /> SSP</h1>
        </Link>
        <ul className='flex gap-x-2'>
            {
                isAuthenticaded? (
                <>
                <li>Bienvenido {user.username.toUpperCase()}</li>
                <li><Link className='bg-indigo-500 px-4 py-1 rounded-sm' to='/vehiculo'>Vehiculos</Link></li>
                <li><Link className='bg-indigo-500 px-4 py-1 rounded-sm' to='/add-vehiculo'>Agregar vehiculo</Link></li>
                <li><Link className='bg-indigo-500 px-4 py-1 rounded-sm' to='/reports'>Reportes</Link></li>
                <li><Link className='bg-red-700 hover:bg-red-600 px-4 py-1 rounded-sm' to='/' onClick={() =>{logout()}}>Logout</Link></li>

                </>):
                (
                    <>
                    <li ><Link className='bg-indigo-500 px-4 py-1 rounded-sm' to='/login'>Login </Link></li>
                    <li ><Link className='bg-indigo-500 px-4 py-1 rounded-sm' to='/register'>Register </Link></li>
                    </>

                )
            }
        </ul>
        

    </nav>
  )
}
